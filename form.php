<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <title>Check in</title>
  </head>
  <body>
    <div class="header">
      <div class="container-fluid">
        <h1>Check in</h1>
      </div>
    </div>
    <div class="body row justify-content-center ">
      <div class="col-5">
        <form action="/" method="post">
          <div class="mb-3">
            <label class="form-label" for="first_name">Full name</label>
            <input class="form-control" name="name">
          </div>
          <div class="mb-3">
            <label class="form-label" name="group">Status</label>
            <select class="form-control" name="group">
              <option value="student">Student</option>
              <option value="teacher">Teacher</option>
              <option value="administator">Administrator</option>
            </select>
          </div>
          <div class="mb-3">
            <label class="form-label" for="age">Age</label>
            <input class="form-control" name="age" type="text"> 
          </div>
          <div class="mb-3">
            <label class="form-label" for="Email">Email</label>
            <input class="form-control" name="email" type="text">
          </div>
          <div class="mb-3">
            <label class="form-label" for="phone">Phone</label>
            <input class="form-control" name="phone" type="text">
          </div>
          <div class="mb-3">
           <button class="btn btn-primary">Отправить</button>
          </div>
        </form>
      </div>
    </div>
    <div class="footer">
      <div class="text-center">
        <p class="footer-text">© 2021</p>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
  </body>
</html>