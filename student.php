<?php
$notes =[
  [
    'name' =>'John Doe',
    'group' =>'student',
    'email' =>'john@gmail.com',
    'phone' =>'123-213-12',
  ],
  [
    'name' =>'Bob Roy',
    'group' =>'student',
    'email' =>'bob@gmail.com',
    'phone' =>'123-456-78',
  ],
  [
    'name' =>'Mark Moore',
    'group' =>'student',
    'email' =>'mark@gmail.com',
    'phone' =>'321-654-87',
  ],
  [
    'name' =>'Indiana Jones',
    'group' =>'student',
    'email' =>'indiana@gmail.com',
    'phone' =>'231-040-45',
  ],
  [
    'name' =>'Robert Anderson',
    'group' =>'student',
    'email' =>'robert@gmail.com',
    'phone' =>'321-557-66',
  ],
  [
    'name' =>'Ava Wilson',
    'group' =>'student',
    'email' =>'ava@gmail.com',
    'phone' =>'457-450-90',
  ],
  [
    'name' =>'Isabella Adrian',
    'group' =>'teacher',
    'email' =>'isabella@gmail.com',
    'phone' =>'546-478-21',
  ],
  [
    'name' =>'Jack Austin',
    'group' =>'teacher',
    'email' =>'jack@gmail.com',
    'phone' =>'547-647-85',
  ],
  [
    'name' =>'Isla Kirk',
    'group' =>'administrator',
    'email' =>'isla@gmail.com',
    'phone' =>'874-475-31',
  ],
  [
    'name' =>'Thomas Backer',
    'group' =>'administrator',
    'email' =>'thomasgmail.com',
    'phone' =>'647-757-65',
  ]
];

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="University database.">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="/css/style.css">
    <title>Student</title>
  </head>
  <body>

    <div class="header">
      <div class="container-fluid">
        <h1>База данных студентов</h1>
      </div>
      <div class="row">
        <div class="col-2">
          <a href="/form.php">Check in</a>
        </div>
        <div class="col-2">
          <a href="/index.php">Homepage</a>
        </div>
        <div class="col-2">
          <a href="/student.php">Student</a>
        </div>
        <div class="col-2">
          <a href="/teacher.php">Teacher</a>
        </div>
        <div class="col-2">
          <a href="/administrator.php">Administrator</a>
        </div>
      </div>
    </div>
    <div class="body row justify-content-center ">
      <div class="col-8"> 
        <table class="table table-striped table-hover">
          <tr>
            <td>#</td>
            <td>Name</td>
            <td>Group</td>
            <td>Email</td>
            <td>Phone</td>
          </tr>
           <!--показываем только студентов --> 
          <?php foreach($notes as$key => $note): ?>
            <?php if('student' == $note['group']): ?>
              <tr>  
                <td><?=++$key; ?></td>
                <td><?=$note['name']; ?></td>
                <td><?=$note['group']; ?></td>
                <td><?=$note['email']; ?></td>
                <td><?=$note['phone']; ?></td>
              </tr>
            <?php endif; ?>
          <?php endforeach; ?>
        </table>
      </div>
    </div>
    <div class="footer">
      <div class="text-center">
        <p class="footer-text">© 2021</p>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
  </body>
</html>